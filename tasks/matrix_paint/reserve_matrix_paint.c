#include <stdio.h>
#include <progbase.h>
#include <pbconsole.h>

int main(void) {
	/* colors encoding table */
	const char colorsTable[16][2] = {
		{'0', BG_BLACK},
		{'1', BG_INTENSITY_BLACK},
		{'2', BG_RED},
		{'3', BG_INTENSITY_RED},
		{'4', BG_GREEN},
		{'5', BG_INTENSITY_GREEN},
		{'6', BG_YELLOW},
		{'7', BG_INTENSITY_YELLOW},
		{'8', BG_BLUE},
		{'9', BG_INTENSITY_BLUE},
		{'A', BG_MAGENTA},
		{'B', BG_INTENSITY_MAGENTA},
		{'C', BG_CYAN},
		{'D', BG_INTENSITY_CYAN},
		{'E', BG_WHITE},
		{'F', BG_INTENSITY_WHITE}
	};
	int colorsTableLength = sizeof(colorsTable) / sizeof(colorsTable[0]);
	char colorsPalette[16] = { '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	int colorsPaletteLength = sizeof(colorsPalette) / sizeof(colorsPalette[0]);
	int i = 0;
	int j = 0;
	int colorPairIndex = 0;
	const char image[28][28] = {
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { 'F','F','F','F','F','F','F','F','F','F','F','F','F','2','2','3','3','6','6','4','4','C','C','8','8','A','A','F' },
    { 'F','F','F','F','F','5','5','5','F','F','F','F','F','F','F','2','2','3','3','6','6','4','4','C','C','8','8','A' },
    { 'F','F','F','5','5','5','0','5','5','F','F','F','F','F','F','F','F','2','2','3','3','6','6','4','4','C','C','8' },
    { 'F','F','5','5','5','5','5','5','5','F','F','F','F','F','F','F','F','F','F','2','2','3','3','6','6','4','4','C' },
    { 'F','F','F','5','5','5','5','5','5','F','F','F','5','5','5','F','F','F','F','F','F','2','2','3','3','6','6','4' },
    { 'F','F','F','F','F','F','5','5','F','F','5','4','5','4','5','4','5','F','F','F','F','F','F','2','2','3','3','6' },
    { 'F','F','F','F','F','F','5','5','5','5','4','5','4','5','4','5','4','5','5','5','F','F','F','F','2','2','3','3' },
    { 'F','F','F','F','F','F','5','5','5','4','5','4','5','4','5','4','5','4','5','5','5','F','F','F','F','F','2','2' },
    { 'F','F','F','F','F','F','5','5','4','5','4','5','4','5','4','5','4','5','4','5','5','5','F','F','F','F','F','F' },
    { 'F','F','F','F','F','F','5','5','5','5','5','5','5','5','5','5','5','5','5','F','5','5','5','F','F','F','F','F' },
    { 'F','F','F','F','F','F','5','5','5','F','F','F','F','F','F','5','5','5','F','F','F','5','5','5','F','F','F','F' },
    { 'F','F','F','F','F','F','5','5','5','F','F','F','F','F','F','5','5','5','F','F','F','F','5','5','F','F','F','F' },
    { '4','4','4','4','4','4','3','3','3','4','4','4','4','4','4','3','3','3','4','4','4','4','4','4','4','4','4','4' },
    { '4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4' },
    { '4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4' },
    { '4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' }
};
	int imageLength = sizeof (image) / sizeof (image[0]);
	const unsigned long MILLIS = 20;
	conClear();
	for (i = 0; i < colorsPaletteLength; i++) {
		char colorCode = '\0';
		char color = '\0';
		/* get current color code from colorsPalette */
		colorCode = colorsPalette[i];
		/* find corresponding color in table */
		for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) {
			char colorPairCode = colorsTable[colorPairIndex][0];
			char colorPairColor = colorsTable[colorPairIndex][1];
			if (colorCode == colorPairCode) {
				color = colorPairColor;
			break;  /* we have found our color, break the loop */
			}
		}
		/* print space with founded color background */
		conSetAttr(color);
		putchar(' ');
	}
	puts("");
	puts("");

                                                                  /*task2*/

	for (i = 0; i < imageLength; i++) {
		for (j = 0; j < imageLength; j++) {
			char colorCode = '\0';
			char color = '\0';
			colorCode = image[i][j];
			for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) {
				char colorPairCode = colorsTable[colorPairIndex][0];
				char colorPairColor = colorsTable[colorPairIndex][1];
				if (colorCode == colorPairCode) {
					color = colorPairColor;
					break;
				}
			}
			conSetAttr(color);
			putchar(' ');
			conReset();
		}
	puts("");
	}
	conReset();
	conClear();

                                                                  /*task3*/

	for (j = 0; j < imageLength; j++) {
		for (i = 0; i < imageLength-1; i++) {
			char colorCode = '\0';
			char color = '\0';
			colorCode = image[i][j];
			for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) {
				char colorPairCode = colorsTable[colorPairIndex][0];
				char colorPairColor = colorsTable[colorPairIndex][1];
				if (colorCode == colorPairCode) {
					color = colorPairColor;
					break;
				}
			}
			conSetAttr(color);
			conMove(i,j+1);
			putchar(' ');
			conReset();
			fflush(stdout);
			sleepMillis(MILLIS);
		}
		conMove(27,j+1);
		conSetAttr(BG_INTENSITY_BLACK);
		putchar(' ');

		j++;

		for (i = imageLength-1; i > 0; i--) {
			char colorCode = '\0';
			char color = '\0';
			colorCode = image[i][j];
			for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) {
				char colorPairCode = colorsTable[colorPairIndex][0];
				char colorPairColor = colorsTable[colorPairIndex][1];
				if (colorCode == colorPairCode) {
					color = colorPairColor;
					break;
				}
			}
			conSetAttr(color);
			conMove(i,j+1);
			putchar(' ');
			conReset();
			fflush(stdout);
			sleepMillis(MILLIS);
		}
	putchar(' ');
	}
	puts("");
	conReset();
	conClear();

                                                                  /*task4*/

	for (i = 0; i < imageLength; i++) {
		for (j = 0; j < imageLength; j++) {
			char colorCode = '\0';
			char color = '\0';
			colorCode = image[imageLength-1-j][i];
			for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) {
				char colorPairCode = colorsTable[colorPairIndex][0];
				char colorPairColor = colorsTable[colorPairIndex][1];
				if (colorCode == colorPairCode) {
					color = colorPairColor;
					break;
				}
			}
			conSetAttr(color);
			putchar(' ');
			conReset();
		}
	puts("");
	}
	puts("");
	conReset();
	return 0;
}
