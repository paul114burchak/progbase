#include <stdio.h>
#include <progbase.h>
#include <pbconsole.h>

int main(void) {
	/* colors encoding table */
	const char colorsTable[16][2] = {
		{'0', BG_BLACK},
		{'1', BG_INTENSITY_BLACK},
		{'2', BG_RED},
		{'3', BG_INTENSITY_RED},
		{'4', BG_GREEN},
		{'5', BG_INTENSITY_GREEN},
		{'6', BG_YELLOW},
		{'7', BG_INTENSITY_YELLOW},
		{'8', BG_BLUE},
		{'9', BG_INTENSITY_BLUE},
		{'A', BG_MAGENTA},
		{'B', BG_INTENSITY_MAGENTA},
		{'C', BG_CYAN},
		{'D', BG_INTENSITY_CYAN},
		{'E', BG_WHITE},
		{'F', BG_INTENSITY_WHITE}
	};
	int colorsTableLength = sizeof(colorsTable) / sizeof(colorsTable[0]);
	char colorsPalette[16] = { '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	int colorsPaletteLength = sizeof(colorsPalette) / sizeof(colorsPalette[0]);
	int i = 0;
	int j = 0;
	int index = 0;
	int x, y;
	int dir = 1;
	int idir = 0;
	int jdir = 1;
	int lim = 0;
	int count = 0;
	int colorPairIndex = 0;
	const char image[28][28] = {
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { 'F','F','F','F','F','F','F','F','F','F','F','F','F','2','2','3','3','6','6','4','4','C','C','8','8','A','A','F' },
    { 'F','F','F','F','F','5','5','5','F','F','F','F','F','F','F','2','2','3','3','6','6','4','4','C','C','8','8','A' },
    { 'F','F','F','5','5','5','0','5','5','F','F','F','F','F','F','F','F','2','2','3','3','6','6','4','4','C','C','8' },
    { 'F','F','5','5','5','5','5','5','5','F','F','F','F','F','F','F','F','F','F','2','2','3','3','6','6','4','4','C' },
    { 'F','F','F','5','5','5','5','5','5','F','F','F','5','5','5','F','F','F','F','F','F','2','2','3','3','6','6','4' },
    { 'F','F','F','F','F','F','5','5','F','F','5','4','5','4','5','4','5','F','F','F','F','F','F','2','2','3','3','6' },
    { 'F','F','F','F','F','F','5','5','5','5','4','5','4','5','4','5','4','5','5','5','F','F','F','F','2','2','3','3' },
    { 'F','F','F','F','F','F','5','5','5','4','5','4','5','4','5','4','5','4','5','5','5','F','F','F','F','F','2','2' },
    { 'F','F','F','F','F','F','5','5','4','5','4','5','4','5','4','5','4','5','4','5','5','5','F','F','F','F','F','F' },
    { 'F','F','F','F','F','F','5','5','5','5','5','5','5','5','5','5','5','5','5','F','5','5','5','F','F','F','F','F' },
    { 'F','F','F','F','F','F','5','5','5','F','F','F','F','F','F','5','5','5','F','F','F','5','5','5','F','F','F','F' },
    { 'F','F','F','F','F','F','5','5','5','F','F','F','F','F','F','5','5','5','F','F','F','F','5','5','F','F','F','F' },
    { '4','4','4','4','4','4','3','3','3','4','4','4','4','4','4','3','3','3','4','4','4','4','4','4','4','4','4','4' },
    { '4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4' },
    { '4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4' },
    { '4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' }
};
	int imageLength = sizeof (image) / sizeof (image[0]);
	conClear();
	for (i = 0; i < colorsPaletteLength; i++) {
		char colorCode = '\0';
		char color = '\0';
		/* get current color code from colorsPalette */
		colorCode = colorsPalette[i];
		/* find corresponding color in table */
		for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) {
			char colorPairCode = colorsTable[colorPairIndex][0];
			char colorPairColor = colorsTable[colorPairIndex][1];
			if (colorCode == colorPairCode) {
				color = colorPairColor;
			break;  /* we have found our color, break the loop */
			}
		}
		/* print space with founded color background */
		conSetAttr(color);
		putchar(' ');
	}
	puts("");
	puts("");

                                                                  /*task2*/

	for (i = 0; i < imageLength; i++) {
		for (j = 0; j < imageLength; j++) {
			char colorCode = '\0';
			char color = '\0';
			colorCode = image[i][j];
			for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) {
				char colorPairCode = colorsTable[colorPairIndex][0];
				char colorPairColor = colorsTable[colorPairIndex][1];
				if (colorCode == colorPairCode) {
					color = colorPairColor;
					break;
				}
			}
			conSetAttr(color);
			putchar(' ');
			conReset();
		}
	puts("");
	}
	conReset();
	

                                                                  /*task3*/
                                             /*Алгоритм вибору способу виведення матриці в консоль*/
	index = 0;
	conMove(1,33);
	printf("Виберіть спосіб виведенння зображення переміщаючись клавішами W та S та натиснувши Пробіл\n");
	conMove(2,33);
	printf("Вертикальна змійка\n");
	conMove(3,33);
	printf("Горизонтальна змійка\n");
	conMove(4,33);
	printf("Діагональна змійка паралельно другорядній діагоналі\n");
	conMove(5,33);
	printf("Діагональна змійка паралельно головній діагоналі\n");
	conMove(6,33);
	printf("Спіраль\n");
	conMove(7,33);
	printf("Обернена спіраль\n");
	while (1) {
		char ch = conGetChar();  /*Зчитування символа з консолі для наступної перевірки*/
		conReset();
		conMove(2,33);
		printf("Вертикальна змійка\n");
		conMove(3,33);
		printf("Горизонтальна змійка\n");
		conMove(4,33);
		printf("Діагональна змійка паралельно другорядній діагоналі\n");
		conMove(5,33);
		printf("Діагональна змійка паралельно головній діагоналі\n");
		conMove(6,33);
		printf("Спіраль\n");
		conMove(7,33);
		printf("Обернена спіраль\n");
		if (ch == 'W' || ch == 'w') {  /*перевірка чи натиснута клавіша W*/
			index--;
		}
		if (ch == 'S' || ch == 's') {  /*перевірка чи натиснута клавіша S*/
			index++;
		}
		if (index < 0) {
			index = 5;
		}
		if (index > 5) {
			index = 0;
		}
		if (index == 0) {                         /*переміщення курсора залежно від натиснутої кнопки*/
			conMove(2,33);
			conSetAttr(FG_BLUE);
			printf("Вертикальна змійка\n");
			conReset();
		}
		if (index == 1) {
			conMove(3,33);
			conSetAttr(FG_BLUE);
			printf("Горизонтальна змійка\n");
			conReset();
		}
		if (index == 2) {
			conMove(4,33);
			conSetAttr(FG_BLUE);
			printf("Діагональна змійка паралельно другорядній діагоналі\n");
			conReset();
		}
		if (index == 3) {
			conMove(5,33);
			conSetAttr(FG_BLUE);
			printf("Діагональна змійка паралельно головній діагоналі\n");
			conReset();
		}
		if (index == 4) {
			conMove(6,33);
			conSetAttr(FG_BLUE);
			printf("Спіраль\n");
			conReset();
		}
		if (index == 5) {
			conMove(7,33);
			conSetAttr(FG_BLUE);
			printf("Обернена спіраль\n");
			conReset();
		}
		if (ch == ' ') {
		conReset();
		conClear();
		switch (index) {
			case 0:
			i = 0;
			j = 0;
			for (count = 0; count < imageLength * imageLength; count++) {
				char colorCode = '\0';
				char color = '\0';
				x = 1 + j;
				y = 1 + i;
				colorCode = image[i][j];
				for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) {
					char colorPairCode = colorsTable[colorPairIndex][0];
					char colorPairColor = colorsTable[colorPairIndex][1];
					if (colorCode == colorPairCode) {
						color = colorPairColor;
						break;
					}
				}
				conSetAttr(color);
				conMove(y,x);
				putchar(' ');
				conReset();
				fflush(stdout);
				sleepMillis(10);
				i += dir;
				
				if (i == imageLength) {
					i--;
					j++;
				dir = -dir;
				}
				if (i == -1) {
					 j++;
					 i++;
				dir = -dir;
				}
			}
			conMove(imageLength+1,1);
				break;
		case 1:
			i = 0;
			j = 0;
			for (count = 0; count < imageLength * imageLength; count++) {
				char colorCode = '\0';
				char color = '\0';
				x = 1 + j;
				y = 1 + i;
				colorCode = image[i][j];
				for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) {
					char colorPairCode = colorsTable[colorPairIndex][0];
					char colorPairColor = colorsTable[colorPairIndex][1];
					if (colorCode == colorPairCode) {
						color = colorPairColor;
						break;
					}
				}
				conSetAttr(color);
				conMove(y,x);
				putchar(' ');
				conReset();
				fflush(stdout);
				sleepMillis(10);
				j += dir;
			
				if (j == imageLength) {
					j--;
					i++;
				dir = -dir;
				}
				if (j == -1) {
					 j++;
					 i++;
				dir = -dir;
				}
			}
		break;
		case 2:
			i = 0;
			j = 0;
			dir = 1;
			for (count = 0; count < imageLength * imageLength; count++) {
				char colorCode = '\0';
				char color = '\0';
				x = 1 + j;
				y = 1 + i;
				colorCode = image[i][j];
				for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) {
					char colorPairCode = colorsTable[colorPairIndex][0];
					char colorPairColor = colorsTable[colorPairIndex][1];
					if (colorCode == colorPairCode) {
						color = colorPairColor;
						break;
					}
				}
				conSetAttr(color);
				conMove(y,x);
				putchar(' ');
				conReset();
				fflush(stdout);
				sleepMillis(10);
				
				i -= dir;
				j += dir;
				
				if (i == imageLength) {
					 i--;
					 j += 2;
					 dir = -dir;
				}
				if (j == imageLength) {
					 j--;
					 i += 2;  
					 dir = -dir;
				}
				if (i == -1) {
					 i++;
					 dir = -dir;
				}
				if (j == -1) {
					 j++;
					 dir = -dir;
				}
			}
		break;
		case 3:
			i = 0;
			j = imageLength-1;
			dir = 1;
			for (count = 0; count < imageLength * imageLength; count++) {
				char colorCode = '\0';
				char color = '\0';
				x = 1 + j;
				y = 1 + i;
				colorCode = image[i][j];
				for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) {
					char colorPairCode = colorsTable[colorPairIndex][0];
					char colorPairColor = colorsTable[colorPairIndex][1];
					if (colorCode == colorPairCode) {
						color = colorPairColor;
						break;
					}
				}
				conSetAttr(color);
				conMove(y,x);
				putchar(' ');
				conReset();
				fflush(stdout);
				sleepMillis(10);
				
				i -= dir;
				j -= dir;
				
				if (i == imageLength) {
					 i--;
					 j -= 2;
					 dir = -dir;
				}
				if (j == imageLength) {
					 j--;
					 dir = -dir;
				}
				if (i == -1) {
					 i++;
					 dir = -dir;
				}
				if (j == -1) {
					 j++;
					 i += 2;
					 dir = -dir;
				}
			}
		break;
		case 4:
			i = 0;
			j = 0;
			idir = 0;
			jdir = 1;
			lim = 0;
			for (count = 0; count < imageLength * imageLength; count++) {
				char colorCode = '\0';
				char color = '\0';
				x = 1 + j;
				y = 1 + i;
				colorCode = image[i][j];
				for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) {
					char colorPairCode = colorsTable[colorPairIndex][0];
					char colorPairColor = colorsTable[colorPairIndex][1];
					if (colorCode == colorPairCode) {
						color = colorPairColor;
						break;
					}
				}
				conSetAttr(color);
				conMove(y,x);
				putchar(' ');
				conReset();
				fflush(stdout);
				sleepMillis(10);
				
				i += idir;
				j += jdir;
				
				if (j == imageLength - lim) {
					 j--;
					 i++;
					 jdir = 0;
					 idir = 1;
				}
				if (i == imageLength - lim) {
					 i--;
					 j--;
					 idir = 0;
					 jdir = -1;
				}
				if (j == -1 + lim) {
					 j++;
					 i--;
					 idir = -1;
					 jdir = 0;
				}
				if (i == lim && j == lim) {
					 lim++;
					 i = j = lim;
					 idir = 0;
					 jdir = 1;
				}
			}
			conMove(imageLength+1,1);
		break;
		case 5:
			i = -1 + imageLength/2;
			j = -1 + imageLength/2;
			idir = 0;
			jdir = 1;
			lim = -1 + imageLength/2;
			for (count = 0; count < imageLength * imageLength; count++) {
				char colorCode = '\0';
				char color = '\0';
				x = 1 + j;
				y = 1 + i;
				colorCode = image[i][j];
				for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) {
					char colorPairCode = colorsTable[colorPairIndex][0];
					char colorPairColor = colorsTable[colorPairIndex][1];
					if (colorCode == colorPairCode) {
						color = colorPairColor;
						break;
					}
				}
				conSetAttr(color);
				conMove(y,x);
				putchar(' ');
				conReset();
				fflush(stdout);
				sleepMillis(10);
				
				i += idir;
				j += jdir;
				
				if (j == imageLength - lim) {
					 j--;
					 i++;
					 jdir = 0;
					 idir = 1;
				}
				if (i == imageLength - lim) {
					 i--;
					 j--;
					 idir = 0;
					 jdir = -1;
				}
				if (j == -1 + lim) {
					 j++;
					 i--;
					 idir = -1;
					 jdir = 0;
				}
				if (i == lim && j == lim) {
					 lim--;
					 i = j = lim;
					 idir = 0;
					 jdir = 1;
				}
			}
			conMove(imageLength+1,1);
		break;
	}
	break;
	}
}
conReset();
puts("");
puts("");
                                                                  /*task4*/

	for (i = 0; i < imageLength; i++) {
		for (j = 0; j < imageLength; j++) {
			char colorCode = '\0';
			char color = '\0';
			colorCode = image[imageLength-1-j][i];
			for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) {
				char colorPairCode = colorsTable[colorPairIndex][0];
				char colorPairColor = colorsTable[colorPairIndex][1];
				if (colorCode == colorPairCode) {
					color = colorPairColor;
					break;
				}
			}
			conSetAttr(color);
			putchar(' ');
			conReset();
		}
	puts("");
	}
	puts("");
	conReset();
	return 0;
}
