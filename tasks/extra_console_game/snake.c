#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pbconsole.h>
#include <progbase.h>

enum { WIDTH = 50 , HEIGHT = 20};

typedef struct point2d {
	int x;
	int y;
} point2d;

typedef struct user {
	char name[30];
	int score;
} user;

void box(void);
void changeDir(int xdir, int ydir, char ch);
void checkBorders(point2d coordinates);
/*void SortUsers(user users[]);
int cmp(const void *a, const void *b);
void AppendUser(user user);*/
void screenClear(void);

int main(void) {
	user user;
	//user users[20];
	point2d coordinates;
	int level = 1;
	char ch = 0;
	int score = 0;

	conClear();
	printf("Input your name:");
	fgets(user.name,30,stdin);
	user.name[strlen(user.name)-1] = '\0';
	conClear();
	conHideCursor();
	printf("Select a level of difficulty");
	conMove(2,1);
	conSetAttr(FG_RED);
	printf("Level %i", level);
	conReset();
	for (int i = 2; i < 10; i++) {
		conMove(i+1,1);
		printf("Level %i", i);
	}

	while (ch != 10) {
		ch = conGetChar();
		if (ch == 119 || ch == 87) {
			level--;
		}
		if (ch == 115 || ch == 83) {
			level++;
		}
		if (level < 1) {
			level = 9;
		}
		if (level > 9) {
			level = 1;
		}
		for (int i = 1; i < 10; i++) {
			conMove(i+1,1);
			printf("Level %i", i);
		}
		conMove(level+1,1);
		conSetAttr(FG_RED);
		printf("Level %i", level);
		conReset();
	}
	conClear();
	box();

	coordinates.x = 2;
	coordinates.y = 2;
	int xdir = 0;
	int ydir = 1;
	int size = 1;

	while (1) {
		conLockInput();
		ch = getchar();
		conUnlockInput();
		changeDir(xdir,ydir,ch);

		for (int i = 0; i < size; i++) {
			coordinates.x += xdir;
			coordinates.y += ydir;

			checkBorders(coordinates);

			conMove(coordinates.y,coordinates.x);
			conSetAttr(BG_YELLOW);
			printf(" ");
			conReset();
		}

		screenClear();

		box();
		sleepMillis(100);

	}

	conClear();
	printf("Your score: %i; Level: %i", user.score,level);
	//AppendUser(user);
	//SortUsers(users);
	return 0;
}

void box(void) {
	conSetAttr(BG_BLUE);
	conMove(1,1);
	for (int i = 0; i < WIDTH+1; i++) {
		printf("#");
	}
	for (int i = 1; i < HEIGHT+1; i++) {
		conMove(i,WIDTH+1);
		printf("#");
	}
	for (int i = 1; i < HEIGHT+1; i++) {
		conMove(i,1);
		printf("#");
	}
	conMove(HEIGHT+1,1);
	for (int i = 1; i < WIDTH+2; i++) {
		printf("#");
	}
	conReset();
}

void changeDir(int xdir, int ydir, char ch) {
	switch (ch) {
		case 'w':
			
				ydir = -1;
				xdir = 0;
			
		break;
		case 'a':
			
				ydir = 0;
				xdir = -1;
			
		break;
		case 's':
			
				ydir = 1;
				xdir = 0;
			
		break;
		case 'd':
			
				xdir = 1;
				ydir = 0;
			
		break;
	}
}

void checkBorders(point2d coordinates) {
	if (coordinates.x < 1) coordinates.x = WIDTH;
	if (coordinates.x > WIDTH) coordinates.x = 1;
	if (coordinates.y < 1) coordinates.y = HEIGHT;
	if (coordinates.y > HEIGHT) coordinates.y = 1;
}

void screenClear(void) {
	conReset();
	conMove(2,2);
	for (int i = 0; i < HEIGHT*HEIGHT; i++) {
		for ( int j = 0; j < WIDTH-2; j++) {
			printf(" ");
		}
	}
}
/*void AppendUser(user user) {
	FILE * fout = fopen("rank.txt", "a");
	fprintf(fout,"%s |%i\n", user.name,user.score);
	fclose(fout);
}

int cmp(const void *a, const void *b) {
	const user left = *(const user *)a; 
	const user right = *(const user *)b; 

	return (left.score < right.score) - (right.score < left.score);
}


void SortUsers(user users[]) {
	char buffer[5000];
	int count = 0;
	FILE * fin = fopen("rank.txt", "r+");
	rewind(fin);
	while (!feof(fin)) {
		fgets(buffer, 5000, fin);
		count++;
	}
	rewind(fin);
	for (int i = 0; i < count; i++) {
		fscanf(fin,"%s |%i\n", &users.name,users.score);
	}
	qsort(users, count, sizeof(user), cmp);
	for (int i = 0; i < count; i++) {
		fprintf(fin,"%s |%i\n", &users.name,users.score);
	}
	fclose(fin);
}*/

