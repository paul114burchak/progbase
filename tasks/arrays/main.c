#include <stdio.h>
#include <math.h>

#include <progbase.h>
#include <pbconsole.h>

int main(void) 
{
	int i = 0;
	int a[10];
	int time();
	int sum = 0;
	int count = 0;
	int min = 0;
	int max = 0;
	int num = 0;
	char s[10];
	
	
	srand(time(NULL));
	
	puts("1. Згенеровано масив А:");
	for (i = 0; i < 10; i++) {
		a[i] = (rand() % 299 )-99;
		printf("%i	", a[i]);
	}
	puts("");
	puts("");
	
	puts("2. Числа в межах [-99..127]:");
	for (i = 0; i < 10; i++) {
		if (a[i] >= -99 && a[i] <= 127) {
			conSetAttr(FG_RED);
			printf("%i	", a[i]);
		} else {
			conReset();
			printf("%i	", a[i]);
		}
	}
	conReset();
	puts("");
	puts("");
	
	puts("3. Числа менші за 50 :");
	for (i = 0; i < 10; i++) {
		if(a[i] < 50) {
			conSetAttr(FG_GREEN);
			printf("%i	", a[i]);
			sum += a[i];
			count++;
		} else {
			conReset();
			printf("%i	", a[i]);
		}
	}
	
	conReset();
	puts("");
	printf("Кількість : %i\n",count);
	printf("Сума : %i\n",sum);
	if (count != 0) {
	printf("Cереднє : %.3f\n",(double) sum/count);
	} else {
	puts("Середнє значення не існує");
	}
	puts("");
	
	puts("4. Значення та індекс першого мінімального числа в межах від [-50..50]: ");
	for (i = 0, min = 51; i < 10; i++) {
		if (a[i] >= -50 && a[i] <= 50 && a[i] < min) {
			min = a[i];
			num = i;
		}
	}
	
	printf("Значення : %i\n",min);
	printf("Індекс : %i\n",num);
	puts("");
	
	puts("5. Значення та індекс першого максимального числа :");
	for (i = 0, max = -100; i < 10; i++) {
		if (a[i] > max) {
			max = a[i];
			num = i;
		}
	}
	
	printf("Значення : %i\n",max);
	printf("Індекс : %i\n",num);
	puts("");
	
	puts("6. Элементи масиву S:");
	for (i = 0; i < 10; i++) {
		s[i] = (abs(a[i]) % 95) + 32;
	}
	s[9] = '\0';
	puts(s);
	puts("");
	
	puts("7. Элементи масиву А, які менші за -25:");
	for (i = 0; i < 10; i++) {
		if (a[i] > -25) {
			a[i] = 0;
			conSetAttr(FG_BLUE);
			printf("%i	", a[i]);
		} else {
			conReset();
			printf("%i	", a[i]);
		}
	}
	conReset();
	puts("");
	return 0;
}
