#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

enum { BUFFER_SIZE = 200 };

int file_process(const char * readFileName, const char * writeFileName);

int main(void) {
	int fp = file_process("in.txt","out.txt");
	if (fp == 1) {
		puts("Error");
	} else {
		puts("Success");
	}
	return 0;
}

int file_process(const char * readFileName, const char * writeFileName) {
	char buffer[BUFFER_SIZE];
	int i = 0;
	FILE * fin = fopen(readFileName, "r");
	FILE * fout = fopen(writeFileName, "w");
	if ( fin == NULL || fout == NULL) {
		return EXIT_FAILURE;
	}
	
	fputs("записати тільки ті непусті рядки, які починаються на число і які містять пробільні символи в собі.\n\n",fout);
	while (!feof(fin)) {
		fgets(buffer, BUFFER_SIZE, fin);
		buffer[strlen(buffer) - 1] = '\0';
		if ( isdigit(buffer[0]) || (buffer[0] == '-' && isdigit(buffer[1]))) {
			int flag = 0;
			for (i = 0 ; i < strlen(buffer); i++) {
				if (isspace(buffer[i])) {
					flag = 1;
					break;
				}
			}
			if (flag == 1) {
				fputs(buffer,fout);
				fputs("\n",fout);
			}
		}
		for ( i = 0; i < strlen(buffer);i++) {
			buffer[i] = '\0';
		}
	}
	fclose(fin);
	fclose(fout);
	return EXIT_SUCCESS;
}
