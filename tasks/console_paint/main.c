#include <stdio.h>

#include <progbase.h>
#include <pbconsole.h>

int main(void) 
{
	int x = 0;
	int y = 0;
	
	conClear();
	conSetAttr(BG_INTENSITY_WHITE);
	
	for ( x = 1; x <= 28; x++) {
		for (y = 1; y <= 16; y++) {
			if (x == 3 || x == 5 || x == 24 || x == 26) {
				for (;y > 8 && y < 13;y++) {
					conSetAttr(BG_BLACK);
					putchar(' ');
				}
			}
			if (x == 6 || x == 23) {
				for (;y > 5 && y < 14;y++) {
					conSetAttr(BG_BLACK);
					putchar(' ');
				}
			}
			conSetAttr(BG_INTENSITY_WHITE);
			conMove(y,x);
			putchar(' ');
		}
	} 
	conSetAttr(BG_BLACK);
	
	conMove(1,7);
	putchar(' ');
	conMove(1,22);
	putchar(' ');
	conMove(2,6);
	putchar(' ');
	conMove(2,8);
	putchar(' ');
	conMove(2,23);
	putchar(' ');
	conMove(2,21);
	putchar(' ');
	conMove(8,4);
	putchar(' ');
	conMove(8,25);
	putchar(' ');
	conMove(13,4);
	putchar(' ');
	conMove(13,25);
	putchar(' ');
	
	x = 7; y = 3;
	conMove(y,x);
	for(;x != 23; x++) {
		conMove(y,x);
		if (x == 7 || x == 9 || (x >=13 && x <= 16) || x == 20 || x == 22) {
		putchar(' ');
		}
	}
	
	x = 8; y++;
	conMove(y,x);
	for(;x != 22; x++) {
		conMove(y,x);
		if (x == 8 || x == 21 || (x >=10 && x <= 12) || (x >=17 && x <= 19)) {
		putchar(' ');
		}
	}
	
	x = 7; y++;
	conMove(y,x);
	for(;x != 23; x++) {
		conMove(y,x);
		if ((x >=7 && x <= 9) || (x >=20 && x <= 22)) {
		putchar(' ');
		}
	}
	
	x = 7; y = 14;
	conMove(y,x);
	for(;x != 23; x++) {
		conMove(y,x);
		if ((x >=7 && x <= 9) || (x >=20 && x <= 22) || (x >=13 && x <= 17)) {
		putchar(' ');
		}
	}
	
	x = 9; y = 15;
	conMove(y,x);
	for(;x != 21; x++) {
		conMove(y,x);
		if (x == 9 || x == 12 || x == 17 || x == 20) {
		putchar(' ');
		}
	}
	
	conMove(16,10);
	putchar(' ');
	conMove(16,11);
	putchar(' ');
	conMove(16,18);
	putchar(' ');
	conMove(16,19);
	putchar(' ');
	conMove(6,7);
	putchar(' ');
	conMove(6,22);
	putchar(' ');
	
	conSetAttr(BG_INTENSITY_GREEN);
	
	y = 2; x = 7;
	conMove(y,x);
	for(;y != 5;y++,x++) {
		conMove(y,x);
		putchar(' ');
		}
	
	y = 2; x = 22;
	conMove(y,x);
	for(;y != 5;y++,x--) {
		conMove(y,x);
		putchar(' ');
		}
	
	y = 9; x = 4;
	conMove(y,x);
	for(;y != 13; y++) {
		conMove(y,x);
		putchar(' ');
	}
	
	for(x = 25, y = 9;y != 13; y++) {
		conMove(y,x);
		putchar(' ');
	}
	
	x = 13; y = 4;
	conMove(y,x);
	for(; x != 17;x++) {
		conMove(y,x);
		putchar(' ');
	}
	
	x = 10; y = 5;
	conMove(y,x);
	for(; x != 20;x++) {
		conMove(y,x);
		putchar(' ');
	}
	
	conMove(7,7);
	for (y = 7; y <= 13; y++) {
		for (x = 7; x <= 22; x++) {
			conMove(y,x);
			putchar(' ');
		}
		conMove(y,x);
	}
	
	x = 8; y = 6;
	conMove(y,x);
	for(; x != 22;x++) {
		conMove(y,x);
		putchar(' ');
	}
	
	conMove(14,10);
	for (x = 10; x <= 19; x++) {
		for (y = 14; y <= 15; y++) {
		if (x == 12) {
			x = 18;
			conMove(y,x);
		}
		conMove(y,x);
		putchar(' ');
		}
	}
	conMove(9,7);
	for(x = 7,y = 9; x <= 22; x++) {
		conSetAttr(BG_BLACK);
		putchar(' ');
	}
	
	conSetAttr(BG_INTENSITY_WHITE);
	conMove(6,12);
	putchar(' ');
	conMove(6,17);
	putchar(' ');
	conMove(13,7);
	putchar(' ');
	
	conReset();
	conMove(17,1);
	return 0;
}
