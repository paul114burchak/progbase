#include <math.h>
#include <stdio.h>

int main(void) 
{
	double x = -10;
	double f1 = 0;
	double f2 = 0;
	
	for (x = -10;x <= 10; x += 0.5) {
		f1 = pow(x,2) - 5;
		f2 = cos(pow(x,2)) + pow(sin(2 * x),2) + 1;
		
		printf("x = %.1f\n", x); 
		printf("F1(x) = %.2f\n", f1);
		printf("F2(x) = %.2f\n", f2);
		printf("F1(x) + F2(x) = %.2f\n", f1 + f2);
		printf("F1(x) * F2(x) = %.2f\n", f1 * f2);
		
		if ( 0 == f2) {
			printf("F1(x) / F2(x) Division by zero\n");
		} else {
			printf("F1(x) / F2(x) = %.2f\n", f1 / f2);
		}
		
		if ( 0 == f1) {
			printf("F2(x) / F1(x) Division by zero\n");
		} else {
			printf("F2(x) / F1(x) = %.2f\n", f2 / f1);
		}
		
		printf("____________________________\n");
		
	}
	return 0;
}
