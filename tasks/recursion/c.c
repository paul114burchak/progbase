#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

char findChAfter(const char * str, char ch);

int main(void) {
	char str[100];
	char ch;
	char start = '0';
	printf("Please, enter a string: ");
	fgets(str, 100, stdin);
	str[strlen(str) - 1] = '\0';
	ch = findChAfter(str, start);
	printf("\nОбчислити і вивести символ, що слідує за останнім символом ‘@’, якщо такого нема - вивести 0 у консоль. \n%c\n", ch);
	return EXIT_SUCCESS;
}

char findChAfter(const char * str, char ch) {
	char firstChar = str[0];
	if (firstChar == '\0') {
		return ch;
	} else { if (firstChar == '@') {
		ch = str[1];
		return findChAfter(str+1,ch);
		} else {
			return findChAfter(str+1,ch);
		}
	}
}
