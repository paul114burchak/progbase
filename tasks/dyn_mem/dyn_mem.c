#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void){
    /* pointers declaration */
	char * inputStr = NULL;
	char * ptr = NULL;
	char * digits = "1234567890";
	
	 /* memory allocation */
	inputStr = (char *) malloc(50 * sizeof(char));           /* alloc memory for input string */
	if (NULL == inputStr) {
		printf("Alloc error");
	return EXIT_FAILURE;
	}
	
	puts("Користувач вводить строку. На основі введеної строки обчислити і вивести останній символ десяткової цифри, або символ ‘0’ якщо такого немає. Вся виділена пам'ять перед закічненням роботи програми повинна бути звільнена.");
	printf("Please, input string: ");
	fgets(inputStr, 51, stdin);
	printf("Your string: %s\n",inputStr);
	
	ptr = strpbrk(inputStr, digits);
	
	if (ptr != NULL) {
		while (strpbrk(ptr + 1, digits) != NULL) {
			ptr = strpbrk(ptr + 1, digits);
		}
		printf("Last decimal digit: %c\n", *ptr);
	} else {
		printf("There are no digits\n0\n");
	}
	
    /* memory deallocation */
	free(inputStr);          /* for malloc */
	
	return EXIT_SUCCESS;
}
