#include <stdio.h>
#include <progbase.h>
#include <pbconsole.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

void reprint (int i);

int menu (int ch, int i, int index);

int inputnum (int ch, int len, int index);

void printMain(void);

int main(void) {
	int ch = 0;
	int cht = 0;
	int index = 0;
	int indext = 0;
	int time();
	int i = 0;
	int j = 0;
	int count;
	int a[10];
	int a2d[8][8];
	float mas1[10];
	float mas2[10];
	const int len = 10;
	const int len2d = 8;
	char str[81] = "Hello, welcome to my lab! I hope u'll like it. Now, choose any option u want";
	char input[10];
	int lenstr;
	int sum;
	char chr;
	conHideCursor();
	
	printMain();
	
	while (ch != 27) {
		ch = conGetChar();
		conReset();
		conMove(2,1);
		printf("Завдання №1. Одномірний масив.\n");
		printf("Завдання №2. Два одномірні масиви.\n");
		printf("Завдання №3. Двомірний масив.\n");
		printf("Завдання №4. Обробка рядків мови С.\n");
		
		index = menu(ch,3,index);
		
		if (ch == 104 || ch == 72) {
			conMove(12,1);
			printf("Help: Головне меню!");
		}
		if (index == 0) {
			conMove(2,1);
			conSetAttr(FG_BLUE);
			printf("Завдання №1. Одномірний масив.\n");
			conReset();
		}
		if (index == 1) {
			conMove(3,1);
			conSetAttr(FG_BLUE);
			printf("Завдання №2. Два одномірні масиви.\n");
			conReset();
		}
		if (index == 2) {
			conMove(4,1);
			conSetAttr(FG_BLUE);
			printf("Завдання №3. Двомірний масив.\n");
			conReset();
		}
		if (index == 3) {
			conMove(5,1);
			conSetAttr(FG_BLUE);
			printf("Завдання №4. Обробка рядків мови С.\n");
			conReset();
		}
		if (ch == 10) {
			conClear();
			switch (index) {
				case 0:
					indext = 0;
					puts("\t\t\t\t\t\t\t\tЗавдання №1");
					printf("1. Заповнити масив випадковими числами від -100 до 100.\n2. Обнулити всі елементи масиву. \n3. Знайти максимальний елемент та його індекс. \n4. Знайти суму елементів масиву.\n");
					printf("5. Вивести суму додатніх елементів масиву. \n6. Знайти перший елемент, що повторюється найбільшу кількість раз. \n7. Поміняти місцями максимальний і мінімальний елементи масиву.\n");
					printf("8. Зменшити всі елементи масиву на введене число.\n");
					conSetAttr(FG_RED);
					puts("___________________________________________________________________________________________________");
					puts("Для переміщення використовуйте клавіші W та S, для вибору Enter. Виклик допомоги h. ESC - вихід.");
					conReset();
					conMove(13,1);
					puts("Масив А:");
					for (i = 0; i < 10; i++) {
						a[i] = 0;
						printf("%i	",a[i]);
					}
					conMove(2,1);
					conSetAttr(FG_BLUE);
					printf("1. Заповнити масив випадковими числами від -100 до 100.\n");
					conReset();
					while(cht != 27) {
						cht = conGetChar();
						conReset();
						conMove(2,1);
						printf("1. Заповнити масив випадковими числами від -100 до 100.\n2. Обнулити всі елементи масиву. \n3. Знайти максимальний елемент та його індекс. \n4. Знайти суму елементів масиву.\n");
						printf("5. Вивести суму додатніх елементів масиву. \n6. Знайти перший елемент, що повторюється найбільшу кількість раз. \n7. Поміняти місцями максимальний і мінімальний елементи масиву.\n");
						printf("8. Зменшити всі елементи масиву на введене число.\n");
						
						indext = menu(cht,7,indext);
						
						if (cht == 104 || cht == 72) {
							conMove(12,1);
							printf("Help:Завдання №1, виберіть потрібну опцію! Вивід масиву виконується в нижній частині консолі. Щоб оновити дані, потрібно вибрати підзавдання ще раз.");
						}
						
						if (indext == 0) {
							conMove(2,1);
							conSetAttr(FG_BLUE);
							printf("1. Заповнити масив випадковими числами від -100 до 100.\n");
							conReset();
						}
						if (indext == 1) {
							conMove(3,1);
							conSetAttr(FG_BLUE);
							printf("2. Обнулити всі елементи масиву. \n");
							conReset();
						}
						if (indext == 2) {
							conMove(4,1);
							conSetAttr(FG_BLUE);
							printf("3. Знайти максимальний елемент та його індекс. \n");
							conReset();
						}
						if (indext == 3) {
							conMove(5,1);
							conSetAttr(FG_BLUE);
							printf("4. Знайти суму елементів масиву.\n");
							conReset();
						}
						if (indext == 4) {
							conMove(6,1);
							conSetAttr(FG_BLUE);
							printf("5. Вивести суму додатніх елементів масиву. \n");
							conReset();
						}
						if (indext == 5) {
							conMove(7,1);
							conSetAttr(FG_BLUE);
							printf("6. Знайти перший елемент, що повторюється найбільшу кількість раз. \n");
							conReset();
						}
						if (indext == 6) {
							conMove(8,1);
							conSetAttr(FG_BLUE);
							printf("7. Поміняти місцями максимальний і мінімальний елементи масиву.\n");
							conReset();
						}
						if (indext == 7) {
							conMove(9,1);
							conSetAttr(FG_BLUE);
							printf("8. Зменшити всі елементи масиву на введене число.\n");
							conReset();
						}
						if (cht == 10) {
							switch (indext) {
								case 0:
									reprint(14);
									srand(time(NULL));
									for (i = 0; i < len; i++) {
										a[i] = (rand() % 201 )-100;
										printf("%i	", a[i]);
									}
								break;
								case 1:
									reprint(14);
									for (i = 0; i < len;i++) {
										a[i] = 0;
										printf("%i	", a[i]);
									}
								break;
								case 2:{
									int max = a[0];
									int posMax = 0;
									for (i = 1; i < len;i++) {
										if ( a[i] > max) {
											max = a[i];
											posMax = i;
										}
									}
									
									reprint(14);
									
									for (i = 0; i < len; i++) {
										if (a[i] == max) {
											conSetAttr(BG_GREEN);
										}
										printf("%i	", a[i]);
										conReset();
									}
									reprint(15);
									printf("Максимальний елемент масиву А: %i. Номер максимального елемента массиву A: %i", max, posMax); 
									}
								break;
								case 3:
									sum = 0;
									for (i = 1; i < len;i++) {
										sum += a[i];
									}
									reprint(16);
									printf("Сума елементів масиву А: %i",sum);
								break;
								case 4:
									sum = 0;
									for (i = 1; i < len;i++) {
										if(a[i] > 0) {
											sum += a[i];
										}
									}
									
									reprint(17);
									printf("Сума додатних елементів масиву А: %i",sum);
								break;
								case 5:
									{
									int maxNum = 0;
									int maxIndex = 0;
									reprint(18);
									for(i = 0; i < len; i++) {
										int chr = a[i];
										count = 0;
										for(j = 0; j < len; j++) {
											if(a[j] == chr)
												count++;
											}
										if (count > maxNum) {
											maxIndex = i;
											maxNum = count;
										} else if(count == maxNum) {
											maxIndex = 0;
										}
									}
									printf("Перший елемент, що повторюється найбільшу кількість раз: %i\n", a[maxIndex]);
									reprint(14);
									
									for(i = 0; i < len; i++){
										if (a[i] == a[maxIndex]) {
											conSetAttr(BG_CYAN);
										}
										printf("%i	", a[i]);
										conReset();
									}
									}
								break;
								case 6:{
									int nummax = -1;
									int nummin = -1;
									int min = a[0];
									int max = a[0];
									reprint(14);
									
									for (i = 1; i < len;i++) {
										if ( a[i] < min) {
											min = a[i];
											nummin = i;
										}
									}
									
									for (i = 1; i < len;i++) {
										if ( a[i] > max) {
											max = a[i];
											nummax = i;
										}
									}
									
									a[nummin] = max;
									a[nummax] = min;
									
									for (i = 0; i < len; i++) {
										if (a[i] == min) {
											conSetAttr(BG_BLUE);
											printf("%i	", a[i]);
											conReset();
										} else {
											if (a[i] == max) {
												conSetAttr(BG_RED);
												printf("%i	", a[i]);
												conReset();
											} else {
												printf("%i	", a[i]);
											}
										}
									}
								}break;
								case 7:
									input[0] = '\0';
									count = 0;
									conMove(19,1);
									conShowCursor();
									printf("Введіть ціле число:");
									reprint(20);
									fgets(input,11,stdin);
									lenstr = strlen(input);
									conMove(20,10);
									printf("                                                               \
                                                                           ");
									for (i = 0; i < lenstr; i++) {
										if ((isdigit(input[0]) || input[0] == '-') && isdigit(input[i])) {
											count++;
										}
									}
									if (lenstr-1 == count) {
										j = atoi(input);
										reprint(14);
										for (i = 0; i < len;i++) {
											a[i] -= j;
											printf("%i	", a[i]);
										}
									} else {
										reprint(20);
										printf("Невірно введені дані, спробуйте ще раз!");
									}
									conHideCursor();
								break;
							}
						}
					}
				cht = 115;
				printMain();
				break;
				case 1:
					indext = 0;
					puts("\t\t\t\t\t\t\t\tЗавдання №2");
					printf("1.Заповнити масиви випадковими дробовими числами від -20.0 до 20.0.\n2.Обнулити всі елементи масивів.\n3.Знайти значення нового масиву чисел, елементи якого будуть сумою відповідних елементів першого і другого масиву.\n");
					printf("4.Вивести номер масиву, у якому максимальна сума елементів.\n5.Поміняти місцями максимальний елемент першого масиву і мінімальний елемент другого масиву.\n");
					conSetAttr(FG_RED);
					puts("___________________________________________________________________________________________________");
					puts("Для переміщення використовуйте клавіші W та S, для вибору Enter. Виклик допомоги h. ESC - вихід.");
					conReset();
					conMove(10,1);
					puts("Масив 1:");
					for (i = 0; i < 10; i++) {
						mas1[i] = 0.0;
						printf("%.3f	",mas1[i]);
					}
					printf("\nМасив 2:\n");
					for (i = 0; i < 10; i++) {
						mas2[i] = 0.0;
						printf("%.3f	",mas2[i]);
					}
					
					conMove(2,1);
					conSetAttr(FG_BLUE);
					printf("1.Заповнити масиви випадковими дробовими числами від -20.0 до 20.0.\n");
					conReset();
					while(cht != 27) {
						cht = conGetChar();
						conReset();
						conMove(2,1);
						printf("1.Заповнити масиви випадковими дробовими числами від -20.0 до 20.0.\n2.Обнулити всі елементи масивів.\n3.Знайти значення нового масиву чисел, елементи якого будуть сумою відповідних елементів першого і другого масиву.\n");
						printf("4.Вивести номер масиву, у якому максимальна сума елементів.\n5.Поміняти місцями максимальний елемент першого масиву і мінімальний елемент другого масиву.\n");
						
						indext = menu(cht,4,indext);
						
						if (cht == 104 || cht == 72) {
							conMove(9,1);
							printf("Help:Завдання №2, виберіть потрібну опцію! Вивід масивів виконується в нижній частині консолі.Щоб оновити дані, потрібно вибрати підзавдання ще раз.");
						}
						if (indext == 0) {
							conMove(2,1);
							conSetAttr(FG_BLUE);
							printf("1.Заповнити масиви випадковими дробовими числами від -20.0 до 20.0.\n");
							conReset();
						}
						if (indext == 1) {
							conMove(3,1);
							conSetAttr(FG_BLUE);
							printf("2.Обнулити всі елементи масивів.\n");
							conReset();
						}
						if (indext == 2) {
							conMove(4,1);
							conSetAttr(FG_BLUE);
							printf("3.Знайти значення нового масиву чисел, елементи якого будуть сумою відповідних елементів першого і другого масиву.\n");
							conReset();
						}
						if (indext == 3) {
							conMove(5,1);
							conSetAttr(FG_BLUE);
							printf("4.Вивести номер масиву, у якому максимальна сума елементів.\n");
							conReset();
						}
						if (indext == 4) {
							conMove(6,1);
							conSetAttr(FG_BLUE);
							printf("5.Поміняти місцями максимальний елемент першого масиву і мінімальний елемент другого масиву.\n");
							conReset();
						}
						if (cht == 10) {
							switch (indext) {
								case 0:
									reprint(11);
									srand(time(NULL));
									for (i = 0; i < len; i++) {
										mas1[i] = (float)(rand())/(float)(RAND_MAX/40)- 20;
										printf("%.3f	", mas1[i]);
									}
									
									reprint(13);
									fflush(stdout);
									sleepMillis(1000);
									srand(time(NULL));
									for (i = 0; i < len; i++) {
										mas2[i] = (float)(rand())/(float)(RAND_MAX/40)- 20;
										printf("%.3f	", mas2[i]);
									}
								break;
								case 1:
									reprint(11);
									for (i = 0; i < 10; i++) {
										mas1[i] = 0.0;
										printf("%.3f	",mas1[i]);
									}
									reprint(13);
									for (i = 0; i < 10; i++) {
										mas2[i] = 0.0;
										printf("%.3f	",mas2[i]);
									}
								break;
								case 2:
									conMove(14,1);
									printf("Новий масив:");
									reprint(15);
									for (i = 0; i < len; i++) {
										printf("%.3f	",mas1[i] + mas2[i]);
									}
								break;
								case 3:{
									float summas1 = 0;
									float summas2 = 0;
									for (i = 0; i < len; i++) {
										summas1 += mas1[i];
										summas2 += mas2[i];
									}
									
									if (summas1 > summas2) {
										reprint(10);
										conSetAttr(BG_MAGENTA);
										printf("Масив 1:");
										conReset();
										conMove(16,1);
										printf("Номер масива з максимальною сумою елементів: 1 (%.5f)", summas1);
									} else { if (summas1 < summas2) {
											reprint(12);
											conSetAttr(BG_MAGENTA);
											printf("Масив 2:");
											conReset();
											conMove(16,1);
											printf("Номер масива з максимальною сумою елементів: 2 (%.5f)", summas2);
										} else {
											conMove(16,1);
											printf("Сума елементів масивів рівна");
										}
									}
									}
								break;
								case 4:
									{
									int nummax = -1;
									int nummin = -1;
									float min = mas2[0];
									float max = mas1[0];
									
									reprint(11);
									reprint(13);
									for (i = 1; i < len;i++) {
										if ( mas2[i] < min) {
											min = mas2[i];
											nummin = i;
										}
									}
									
									for (i = 1; i < len;i++) {
										if ( mas1[i] > max) {
											max = mas1[i];
											nummax = i;
										}
									}
									
									mas2[nummin] = max;
									mas1[nummax] = min;
									
									for (i = 0; i < len; i++) {
										if (mas1[i] == min) {
											conMove(11,1 + (i*8));
											conSetAttr(BG_BLUE);
											printf("%.3f	", mas1[i]);
											conReset();
											conMove(13,1 + (i*8));
											printf("%.3f	", mas2[i]);
										} else {
											if (mas2[i] == max) {
												conMove(13,1 + (i*8));
												conSetAttr(BG_RED);
												printf("%.3f	", mas2[i]);
												conReset();
												conMove(11,1 + (i*8));
												printf("%.3f	", mas1[i]);
											} else {
												conMove(11,1 + (i*8));
												printf("%.3f	", mas1[i]);
												conMove(13,1 + (i*8));
												printf("%.3f	", mas2[i]);
											}
										}
									}
									}
								break;
							}
						}
					}
				cht = 115;
				printMain();
				break;
				case 2:
					indext = 0;
					puts("\t\t\t\t\t\t\t\tЗавдання №3");
					printf("1.Заповнити масив випадковими числами від -10 до 10.\n2.Обнулити всі елементи масиву.\n3.Знайти мінімальний елемент та його індекси.\n4.Знайти суму головної діагоналі масиву.\n");
					printf("5.Знайти суму рядка за заданим індексом.\n6.Поміняти місцями максимальний і мінімальний елементи масиву.\n7.Змінити значення елементу за вказаними індексами на задане.\n");
					conSetAttr(FG_RED);
					puts("___________________________________________________________________________________________________");
					puts("Для переміщення використовуйте клавіші W та S, для вибору Enter. Виклик допомоги h. ESC - вихід.");
					conReset();
					conMove(12,1);
					puts("Двомірний масив:");
					for (i = 0;i < len2d; i++) {
						for (j = 0; j < len2d; j++) {
							a2d[i][j] = 0;
							printf("%i	",a2d[i][j]);
						}
						puts("");
					}
					conMove(2,1);
					conSetAttr(FG_BLUE);
					printf("1.Заповнити масив випадковими числами від -10 до 10.\n");
					conReset();
					while(cht != 27) {
						cht = conGetChar();
						conReset();
						conMove(2,1);
						printf("1.Заповнити масив випадковими числами від -10 до 10.\n2.Обнулити всі елементи масиву.\n3.Знайти мінімальний елемент та його індекси.\n4.Знайти суму головної діагоналі масиву.\n");
						printf("5.Знайти суму рядка за заданим індексом.\n6.Поміняти місцями максимальний і мінімальний елементи масиву.\n7.Змінити значення елементу за вказаними індексами на задане.\n");
						
						indext = menu(cht,6,indext);
						
						if (cht == 104 || cht == 72) {
							conMove(11,1);
							printf("Завдання №3,виберіть потрібну опцію!Робота з двомірним масивом виводиться в нижній частині.Щоб оновити дані, потрібно вибрати підзавдання ще раз.");
						}
						if (indext == 0) {
							conMove(2,1);
							conSetAttr(FG_BLUE);
							printf("1.Заповнити масив випадковими числами від -10 до 10.\n");
							conReset();
						}
						if (indext == 1) {
							conMove(3,1);
							conSetAttr(FG_BLUE);
							printf("2.Обнулити всі елементи масиву.\n");
							conReset();
						}
						if (indext == 2) {
							conMove(4,1);
							conSetAttr(FG_BLUE);
							printf("3.Знайти мінімальний елемент та його індекси.\n");
							conReset();
						}
						if (indext == 3) {
							conMove(5,1);
							conSetAttr(FG_BLUE);
							printf("4.Знайти суму головної діагоналі масиву.\n");
							conReset();
						}
						if (indext == 4) {
							conMove(6,1);
							conSetAttr(FG_BLUE);
							printf("5.Знайти суму рядка за заданим індексом.\n");
							conReset();
						}
						if (indext == 5) {
							conMove(7,1);
							conSetAttr(FG_BLUE);
							printf("6.Поміняти місцями максимальний і мінімальний елементи масиву.\n");
							conReset();
						}
						if (indext == 6) {
							conMove(8,1);
							conSetAttr(FG_BLUE);
							printf("7.Змінити значення елементу за вказаними індексами на задане.\n");
							conReset();
						}
						if (cht == 10) {
							switch (indext) {
								case 0:
									for (i = 13; i < 21; i++) {
										reprint(i);
									}
									conMove(13,1);
									for (i = 0;i < len2d; i++) {
										for (j = 0; j < len2d; j++) {
											a2d[i][j] = (rand() % 21)-10;
											printf("%i	",a2d[i][j]);
										}
										puts("");
									}
								break;
								case 1:
									for (i = 13; i < 21; i++) {
										reprint(i);
									}
									conMove(13,1);
									for (i = 0;i < len2d; i++) {
										for (j = 0; j < len2d; j++) {
											a2d[i][j] = 0;
											printf("%i	",a2d[i][j]);
										}
										puts("");
									}
								break;
								case 2:{
									int min = a2d[0][0];
									count = 0;
									conMove(13,1);
									
									for (i = 0;i < len2d; i++) {
										for (j = 0; j < len2d; j++) {
											if (a2d[i][j] < min) {
												min = a2d[i][j];
											}
										}
									}
									reprint(21);
									printf("Індекси мінімальних елементів:  ");
									
									for (i = 0;i < len2d; i++) {
										for (j = 0; j < len2d; j++) {
											if (a2d[i][j] == min) {
												count++;
												conMove(21,31+(8*(count-1)));
												printf("%i:(%i,%i)",count,j,i);
												conSetAttr(BG_BLUE);
												conMove(13+i,1+(8*j));
												printf("%i	",a2d[i][j]);
												conReset();
											}
										}
									}
								}break;
								case 3:
									sum = 0;
									for (i = 0;i < len2d; i++) {
										sum += a2d[i][i];
										conMove(13+i,1+(i*8));
										conSetAttr(BG_YELLOW);
										printf("%i	",a2d[i][i]);
										conReset();
									}
									reprint(22);
									printf("Cума елементів головної діагоналі: %i",sum);
								break;
								case 4:{
									int ch3 = 0;
									i = 0;
									sum = 0;
									reprint(23);
									printf("Введіть iндекс рядка:  %i  ", i);
									conMove(23,30);
									conSetAttr(FG_RED);
									printf("Використовуйте W та S щоб встановити потрібний індекс рядка");
									conReset();
									while ( ch3 != 10) {
										ch3 = conGetChar();
										
										i = inputnum(ch3,len2d-1,i);
										
										conMove(23,1);
										printf("Введіть iндекс рядка:  %i  ", i);
									}
									
									conMove(13+i,1);
									for (j = 0; j < len2d; j++) {
										sum += a2d[i][j];
										conSetAttr(BG_GREEN);
										printf("%i	",a2d[i][j]);
										conReset();
									}
									
									reprint(24);
									printf("Cума %i-го рядка: %i",i,sum);
								} break;
								case 5:{
									int rowmin = 0;
									int colmin = 0;
									int rowmax = 0;
									int colmax = 0;
									int min = a2d[0][0];
									int max = a2d[0][0];
									
									for (i = 13; i < 21; i++) {
										reprint(i);
									}
									conMove(13,1);
									
									for (i = 1; i < len2d; i++) {
										for (j = 1; j < len2d; j++) {
											if ( a2d[i][j] < min) {
												min = a2d[i][j];
												rowmin = i;
												colmin = j;
											}
										}
									}
									
									for (i = 1; i < len2d; i++) {
										for (j = 1; j < len2d; j++) {
											if ( a2d[i][j] > max) {
												max = a2d[i][j];
												rowmax = i;
												colmax = j;
											}
										}
									}
									
									a2d[rowmin][colmin] = max;
									a2d[rowmax][colmax] = min;
									
									for (i = 0; i < len2d; i++) {
										for (j = 0; j < len2d; j++) {
											if (a2d[i][j] == min) {
												conSetAttr(BG_BLUE);
												printf("%i	", a2d[i][j]);
												conReset();
											} else {
												if (a2d[i][j] == max) {
													conSetAttr(BG_RED);
													printf("%i	", a2d[i][j]);
													conReset();
												} else {
													printf("%i	", a2d[i][j]);
												}
											}
										}
										puts("");
									}
								} break;
								case 6:{
									int ch3 = 0;
									int indx = 0;
									i = 0;
									reprint(25);
									printf("Введіть iндекс рядка:  %i  ", i);
									conMove(26,1);
									conSetAttr(FG_RED);
									printf("Використовуйте W та S щоб встановити потрібні індекси елемету");
									conReset();
									while (ch3 != 10) {
										ch3 = conGetChar();
										
										i = inputnum(ch3,len2d-1,i);
										
										conMove(25,1);
										printf("Введіть iндекс рядка:  %i  ", i);
									}
									
									ch3 = 0;
									j = 0;
									conMove(25,30);
									printf("Введіть iндекс стовпця:  %i  ", j);
									while (ch3 != 10) {
										ch3 = conGetChar();
										
										j = inputnum(ch3,len2d-1,j);
										
										conMove(25,30);
										printf("Введіть iндекс стовпця:  %i  ", j);
									}
									
									input[0] = '\0';
									count = 0;
									conMove(27,1);
									conShowCursor();
									printf("Введіть ціле число:");
									reprint(28);
									fgets(input,11,stdin);
									lenstr = strlen(input);
									conMove(28,10);
									printf("                                                               \
                                                                           ");
									for (indx = 0; indx < lenstr; indx++) {
										if ((isdigit(input[0]) || input[0] == '-') && isdigit(input[indx])) {
											count++;
										}
									}
									if (lenstr-1 == count) {
										a2d[i][j] = atoi(input);
										conMove(13+i,1+(8*j));
										conSetAttr(BG_CYAN);
										printf("%i 	", a2d[i][j]);
										conReset();
									} else {
										reprint(28);
										printf("Невірно введені дані, спробуйте ще раз!");
									}
									conHideCursor();
								}break;
							}
						}
					}
				cht = 115;
				printMain();
				break;
				case 3:
					indext = 0;
					puts("\t\t\t\t\t\t\t\tЗавдання №4");
					printf("1.Заповнити строку вводом із консолі.\n2.Очистити строку.\n3.Вивести довжину строки.\n4.Вивести підстроку із заданої позиції і заданої довжини.\n5.Вивести список підстрок, розділених заданим символом.");
					printf("\n6.Вивести найдовше слово (слова - непуста послідовність буквенних символів).\n7.Знайти та вивести всі цілі числа, що містяться у строці.\n8.Знайти та вивести суму всіх дробових чисел, що містяться у строці.\n");
					conSetAttr(FG_RED);
					puts("___________________________________________________________________________________________________");
					puts("Для переміщення використовуйте клавіші W та S, для вибору Enter. Виклик допомоги h. ESC - вихід.");
					conReset();
					puts("");
					puts("");
					
					puts(str);
					
					conMove(2,1);
					conSetAttr(FG_BLUE);
					printf("1.Заповнити строку вводом із консолі.\n");
					conReset();
					while(cht != 27) {
						cht = conGetChar();
						conReset();
						conMove(2,1);
						printf("1.Заповнити строку вводом із консолі.\n2.Очистити строку.\n3.Вивести довжину строки.\n4.Вивести підстроку із заданої позиції і заданої довжини.\n5.Вивести список підстрок, розділених заданим символом.");
						printf("\n6.Вивести найдовше слово (слова - непуста послідовність буквенних символів).\n7.Знайти та вивести всі цілі числа, що містяться у строці.\n8.Знайти та вивести суму всіх дробових чисел, що містяться у строці.\n");
						
						indext = menu(cht,7,indext);
						
						if (cht == 104 || cht == 72) {
							conMove(12,1);
							printf("Завдання №4, виберіть потрібну опцію! Робота зі строкою виводиться в нижній частині консолі. Щоб оновити дані, потрібно вибрати підзавдання ще раз.");
						}
						if (indext == 0) {
							conMove(2,1);
							conSetAttr(FG_BLUE);
							printf("1.Заповнити строку вводом із консолі.\n");
							conReset();
						}
						if (indext == 1) {
							conMove(3,1);
							conSetAttr(FG_BLUE);
							printf("2.Очистити строку.\n");
							conReset();
						}
						if (indext == 2) {
							conMove(4,1);
							conSetAttr(FG_BLUE);
							printf("3.Вивести довжину строки.\n");
							conReset();
						}
						if (indext == 3) {
							conMove(5,1);
							conSetAttr(FG_BLUE);
							printf("4.Вивести підстроку із заданої позиції і заданої довжини.\n");
							conReset();
						}
						if (indext == 4) {
							conMove(6,1);
							conSetAttr(FG_BLUE);
							printf("5.Вивести список підстрок, розділених заданим символом.\n");
							conReset();
						}
						if (indext == 5) {
							conMove(7,1);
							conSetAttr(FG_BLUE);
							printf("6.Вивести найдовше слово (слова - непуста послідовність буквенних символів).\n");
							conReset();
						}
						if (indext == 6) {
							conMove(8,1);
							conSetAttr(FG_BLUE);
							printf("7.Знайти та вивести всі цілі числа, що містяться у строці.\n");
							conReset();
						}
						if (indext == 7) {
							conMove(9,1);
							conSetAttr(FG_BLUE);
							printf("8.Знайти та вивести суму всіх дробових чисел, що містяться у строці.\n");
							conReset();
						}
						if (cht == 10) {
							switch (indext) {
								case 0:
									conShowCursor();
									conMove(13,1);
									printf("Введіть що-небудь (80 символів)");
									reprint(14);
									fgets(str,81,stdin);
									conMove(14,81);
									printf("                                                                    ");
									conHideCursor();
									reprint(13);
								break;
								case 1:
									reprint(14);
									for (j = 0; j < lenstr; j++) {
										str[j] = '\0';
									}
								break;
								case 2:
									reprint(15);
									lenstr = strlen(str);
									printf("Довжина строки: %i символів", lenstr);
								break;
								case 3:{
									int ch4 = 0;
									lenstr = strlen(str);
									i = 0;
									reprint(16);
									printf("Введіть позицію:  %i  ", i);
									conMove(16,50);
									conSetAttr(FG_RED);
									printf("Використовуйте W та S щоб встановити потрібну позицію та довжину строки");
									conReset();
									while ( ch4 != 10) {
										ch4 = conGetChar();
										
										i = inputnum(ch4,lenstr-1,i);
										
										if (lenstr == 0) {
											i = 0;
										}
										conMove(16,1);
										printf("Введіть позицію:  %i  ", i);
									}
									
									ch4 = 0;
									j = 0;
									conMove(16,25);
									printf("Введіть довжину:  %i  ", j);
									while ( ch4 != 10) {
										ch4 = conGetChar();
										
										j = inputnum(ch4,lenstr-i,j);
										
										conMove(16,25);
										printf("Введіть довжину:  %i  ", j);
									}
									
									reprint(17);
									for ( count = i; count < i+j; count++) {
										printf("%c",str[count]);
									}
								} break;
								case 4:
									count = 0;
									j = 0;
									lenstr = strlen(str);
									conShowCursor();
									conMove(18,1);
									printf("Введіть будь-який символ, що міститься в строці: ");
									chr = conGetChar();
									printf("%c",chr);
									reprint(19);
									reprint(20);
									reprint(19);
									conSetAttr(FG_GREEN);
									printf("1-а підстрока: ");
									conReset();
									for (i = 0; i < lenstr; i++) {
										if (chr == str[i]) {
											j++;
											conSetAttr(FG_GREEN);
											printf("   %i-а п-стр.: ",j+1);
											conReset();
											if ( j == 5) {
												printf("\n");
											}
										} else {
											printf("%c",str[i]);
											count++;
										}
									}
									
									if (count == lenstr) {
										reprint(19);
										printf("Такого символу в строці немає");
									}
									if ((j > lenstr/2) && (lenstr > 40)) {
										reprint(19);
										printf("Why did you make this? It hasn't got any sence");
										for (i = 20; i < 30; i++) {
											reprint(i);
										}
									}
									conHideCursor();
								break;
								case 5:{
									int max = 0;
									int start = 0;
									int finish = 0;
									lenstr = strlen(str);
									reprint(21);
									printf("Найдовше слово в строці: ");
									count = 0;
									for (i = 0; i < lenstr; i++) {
										if (isalpha(str[i])) {
											for (j = i, count = 0; isalpha(str[j]);j++) {
												count++;
											}
											
											if (count > max) {
												max = count;
												start = i;
												finish = j;
											}
										i = j;
										}
									}
									
									for (;start < finish; start++) {
										printf("%c",str[start]);
									}
									printf("  Довжина: %i",max);
									}
								break;
								case 6:
									reprint(22);
									lenstr = strlen(str);
									for (i = 0; i < lenstr; i++) {
										char num = str[i];
										char nextCh = str[i + 1];
										if (isdigit(num) || ('-' == num && isdigit(nextCh))) {
											int number = atoi(str + i);
											count = printf("%i", number);
											i += count - 1;
										}
									}
								break;
								case 7:{
									float sumstr = 0;
									int point = 0;
									int temp = 0;
									char add[81];
									add[0] ='\0';
									reprint(23);
									lenstr = strlen(str);
									for (i = 0; i < lenstr; i++) {
										add[0] ='\0';
										if ( isdigit(str[i]) || (str[i] == '-' && isdigit(str[i+1]))) {
											if (str[i] =='-') {
												temp = i;
												i++;
											}
											for (j = i, count = 0; isdigit(str[j]); j++, count++) {
												add[count] = str[j];
											}
											i = j;
											if ( str[j] == '.' ) {
												add[count] = str[j];
												count += 1;
												for (point = j+1; isdigit(str[point]); point++, count++) {
													add[count] = str[point];
												}
												i = point;
											}
											if (str[temp] == '-') {
												sumstr += -(atof(add));
											} else {
												sumstr += atof(add);
											}
											for (j = 0; j < lenstr; j++) {
												add[j] = '\0';
											}
											temp = -1;
										}
										
									}
									printf("Сума всіх дробових чисел: %f", sumstr);
								} break;
							}
						}
					}
				cht = 115;
				printMain();
				break;
			}
		}
	}
	conClear();
	conShowCursor();
	puts("Good Bye!");
	return 0;
}

void printMain(void) {
	conClear();
	conMove(1,1);
	printf("\t\t\t\t\t\t\t\tЛабораторна робота №2\n");
	printf("Завдання №1. Одномірний масив.\nЗавдання №2. Два одномірні масиви.\n\
Завдання №3. Двомірний масив.\nЗавдання №4. Обробка рядків мови С.\n");
	conSetAttr(FG_RED);
	puts("___________________________________________________________________________________________________");
	puts("Для переміщення використовуйте клавіші W та S, для вибору Enter. Виклик допомоги h. ESC - вихід.");
	conMove(2,1);
	conSetAttr(FG_BLUE);
	printf("Завдання №1. Одномірний масив.\n");
	conReset();
}

void reprint (int i) {
	conMove(i,1);
	printf("                                                               \
                                                                                     ");
	conMove(i,1);
}

int menu (int ch, int i, int index) {
	if (ch == 119 || ch == 87) {
		index--;
	} 
	if (ch == 115 || ch == 83) {
		index++;
	} 
	if (index < 0) {
		index = i;
	}
	if (index > i) {
		index = 0;
	}
	return index;
}

int inputnum (int ch, int len, int index) {
	if (ch == 119 || ch == 87) {
		index++;
	}
	if (ch == 115 || ch == 83) {
		index--;
	}
	if (index < 0) {
		index = len;
	}
	if (index > len) {
		index = 0;
	}
	return index;
}
