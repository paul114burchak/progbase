#include <stdio.h>
#include <progbase.h>
#include <pbconsole.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "struct.h"

void menuPrint(void) {
	conClear();
	conMove(1,1);
	printf("\t\t\t\t\t\tЛабораторна робота №3 (Інтернет-провайдер)\n");
	conMove(3,30);
	conSetAttr(FG_RED);
	printf("Створити новий масив даних");
	conReset();
	conMove(3,93);
	printf("Зчитати масив даних із файлу");
	conHideCursor();
}

void funcsPrint(struct internet_prov provider[],int i) {
	int k = -1;
	for (int j = 0; j < i; j++) {
		k++;
		if (k > 2) {
			k = 0;
		}
		int p = 0;
		if (j < 3) {
			p = 0;
		}
		if (j >= 3 && j < 6) {
			p = 1;
		}
		if (j >= 6 && j < 9) {
			p = 2;
		}
		if (j >= 9) {
			p = 3;
		}
		conSetAttr(FG_RED);
		conMove(1+(7*p),(k*50)+1);
		printf("Cтруктура №%i:",j+1);
		conReset();
		conMove(2+(7*p),(k*50)+1);
		printf("1) Назва:%s", provider[j].name);
		conMove(3+(7*p),(k*50)+1);
		printf("2) Швидкість:%i Mбіт/сек", provider[j].speed);
		conMove(4+(7*p),(k*50)+1);
		printf("3) Тариф:%.3f грн/міс", provider[j].tariff);
		conMove(5+(7*p),(k*50)+1);
		printf("4.1) Ім'я директора:%s", provider[j].boss.fullname);
		conMove(6+(7*p),(k*50)+1);
		printf("4.2) Вік:%i", provider[j].boss.age);
		conMove(7+(7*p),(k*50)+1);
		printf("4.3) Стать:%s", provider[j].boss.sex);
	}
}


void exitlab(void) {
	printf("\nНатисніть Backspace, щоб вийти в головне меню\n");
	int chs = 0;
	while (chs != 127) {
		chs = conGetChar();
	}
}

void printTaskMenu(void) {
	conMove(1,1);
	printf("\t\t\t\t\t\tЛабораторна робота №3 (Інтернет-провайдер)\n");
	conMove(5,5);
	printf("StructsToTextfile (quantity)");
	conMove(6,5);
	printf("TextfileToStructs (quantity)");
	conMove(7,5);
	printf("DeleteStruct");
	conMove(8,5);
	printf("CopyStruct");
	conMove(9,5);
	printf("RewriteStructField(pointer)");
	conMove(10,5);
	printf("Знайти К провайдерів із найдорожчим тарифом на інтернет");
	conMove(11,5);
	printf("Existed structs to textfile");
	conMove(12,5);
	printf("Existed structs on screen");
	conMove(13,5);
	printf("RewriteOneStructByIndex");
}
