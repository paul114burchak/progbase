struct Boss {
		char fullname[30];
		unsigned int age;
		char sex[30];
	};
struct internet_prov {
	char name[30];
	unsigned int speed;
	float tariff;
	struct Boss boss;
};

void createNew(char input[][30]);
void taskMenu(struct internet_prov provider[], int lim);
void StrToStruct(struct internet_prov provider[], int j, char str[][30]);
char * StructToStr(struct internet_prov * provider, char * str);
int TextToStructs(struct internet_prov providers[], int i, const char * readFileName);
int StructsToText(struct internet_prov providers[], int i, const char * writeFileName);
void DeleteStruct(struct internet_prov providers[], int j);
void RewriteStruct(struct internet_prov providers[], int i, int j);
void RewriteStructField(struct internet_prov * provider, int field, char * str);
void Top_K_Structs(struct internet_prov providers[], int k, int lim);
int cmp(const void *a, const void *b);
