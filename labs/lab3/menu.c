#include <stdio.h>
#include <progbase.h>
#include <pbconsole.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "struct.h"
#include "prints.h"
#include "menu.h"

void taskMenu(struct internet_prov providers[], int lim) {
	int cht = 0;
	int indext = 0;
	conClear();
	conHideCursor();
	conMove(4,5);
	conSetAttr(FG_RED);
	printf("StructToStr(pointer)");
	conReset();
	printTaskMenu();
	while (cht != 127) {
		cht = conGetChar();
		conReset();
		conMove(4,5);
		printf("StructToStr(pointer)");
		printTaskMenu();
		indext = menu(cht,9,indext,83,87);
		ifsMenu(indext);
		if (cht == 10) {
			switch (indext) {
				case 0: {
					int i = 1;
					char str[200];
					i = countStruct(i,lim,1);
					(void)StructToStr(&providers[i-1], str);
					conMove(6,3);
					puts(str);
					backToMenu();
				} break;
				case 1: {
					int i = 1;
					char str[6][30];
					char filename[20];
					int f = 0;
					i = countStruct(i,10,1);
					conClear();
					conShowCursor();
					for (int j = 0; j < i;j++) {
						conMove(1,1);
						printf("Введіть структури, заповняючи поля в такому порядку: \n1) Назва:\n2) Швидкість:\n3) Тариф:\n4.1) Ім'я директора:\n4.2) Вік:\n4.3) Стать:");
						conMove(2,1);
						createNew(str);
						StrToStruct(providers, j, str);
						conClear();
					}
					printf("Введіть назву файлу в який будуть записані дані: ");
					fgets(filename,20,stdin);
					filename[strlen(filename) - 1] = '\0';
					f = StructsToText(providers,i, filename);
					if (f == 1) {
						printf("Smth wrong");
					}
					if(i > lim) {
						lim = i;
					}
					conHideCursor();
					backToMenu();
				} break;
				case 2: {
					int i = 1;
					int f = 0;
					i = countStruct(i,10,1);
					conMove(1,1);
					printf("                                     ");
					char filename[20];
					printf("Введіть назву файлу з якого будуть зчитані дані (structs.txt): ");
					fgets(filename,20,stdin);
					filename[strlen(filename) - 1] = '\0';
					f = TextToStructs(providers,i,filename);
					conClear();
					if (f == 1) {
						printf("Файл пустий або не існує");
					} else {
						if(i > lim) {
							lim = i;
						}
						funcsPrint(providers, i);
					}
					backToMenu();
				} break;
				case 3: {
					int i = 1;
					i = countStruct(i,lim,1);
					DeleteStruct(providers, i-1);
					conMove(5,1);
					printf("Дані зі Структури №%i очищені", i);
					backToMenu();
				} break;
				case 4: {
					int i = 1;
					int j = 1;
					int ch = 0;
					i = countStruct(i,lim,1);
					conMove(4,1);
					printf("Введіть індекс структури, що копіюється:  %i  ", j);
					while ( ch != 10) {
						ch = conGetChar();
						j = FuncMenu(ch,j,lim);
						conMove(4,1);
						printf("Введіть індекс структури, що копіюється:  %i  ", j);
					}
					RewriteStruct(providers,i-1,j-1);
					backToMenu();
				} break;
				case 5: {
					int i = 1;
					int field = 0;
					int ch = 0;
					char str[30];
					i = countStruct(i,lim,1);
					conMove(4,1);
					printf("0-Назва;1-Швидкість;2-Тариф(float);3-Ім'я директора;4-Вік;5-Стать\n");
					printf("Номер поля:  %i  ",field);
					while ( ch != 10) {
						ch = conGetChar();
						field = menu(ch, 5, field, 87, 83);
						conMove(5,1);
						printf("Номер поля:  %i  ",field);
					}
					conMove(6,1);
					printf("Нове значення поля: ");
					fgets(str,30,stdin);
					str[strlen(str) - 1] = '\0';
					RewriteStructField(&providers[i-1], field, str);
					backToMenu();
				} break;
				case 6:{
					int k = 1;
					int ch = 0;
					conClear();
					conMove(1,1);
					printf("K = %i  \n", k);
					conSetAttr(FG_RED);
					printf("Використовуйте W та S для вибору");
					conReset();
					while ( ch != 10) {
						ch = conGetChar();
						k = FuncMenu(ch,k,lim);
						conMove(1,1);
						printf("K = %i  ", k);
					}
					Top_K_Structs(providers, k, lim);
					conClear();
					if (lim!=0) funcsPrint(providers, k);
					backToMenu();
				} break;
				case 7: {
					char filename[20];
					int f = 0;
					conClear();
					printf("Введіть назву файлу в який будуть записані дані: ");
					fgets(filename,20,stdin);
					filename[strlen(filename) - 1] = '\0';
					StructsToText(providers,lim, filename);
					if (f == 1) {
						printf("Smth wrong");
					}
					backToMenu();
				} break;
				case 8: {
					conClear();
					funcsPrint(providers, lim);
					backToMenu();
				} break;
				case 9: {
					int i = 1;
					char str[6][30];
					i = countStruct(i,lim,1);
					conClear();
					conShowCursor();
					conMove(1,1);
					printf("Введіть структурy, заповняючи поля в такому порядку: \n1) Назва:\n2) Швидкість:\n3) Тариф:\n4.1) Ім'я директора:\n4.2) Вік:\n4.3) Стать:");
					conMove(2,1);
					createNew(str);
					StrToStruct(providers, i-1, str);
					conClear();
					conHideCursor();
					backToMenu();
				} break;
			}
			indext = 0;
		}
	}
	menuPrint();
}

int FuncMenu (int ch, int j, int lim) {
	if (ch == 119 || ch == 87) {
		j++;
	}
	if (ch == 115 || ch == 83) {
		j--;
	}
	if (j < 1) {
		j = lim;
	}
	if (j > lim) {
		j = 1;
	}
	return j;
}

int menu (int ch, int i, int index,int butP, int butM) {
	if (ch == butP || ch == (butP+32)) {
		index++;
	}
	if (ch == butM || ch == (butM+32)) {
		index--;
	}
	if (index < 0) {
		index = i;
	}
	if (index > i) {
		index = 0;
	}
	return index;
}

int countStruct (int index, int h, int l) {
	int ch = 0;
	conClear();
	conMove(1,1);
	printf("\t\t\t\t\t\tЛабораторна робота №3 (Інтернет-провайдер)\n");
	printf("Введіть кількість(номер) структур(и):  %i  ", index);
	conMove(3,10);
	conSetAttr(FG_RED);
	printf("Використовуйте W та S для вибору");
	conReset();
	while ( ch != 10) {
		ch = conGetChar();
		if (ch == 119 || ch == 87) {
			index++;
		}
		if (ch == 115 || ch == 83) {
			index--;
		}
		if (index < l) {
			index = h;
		}
		if (index > h) {
			index = l;
		}
		conMove(2,1);
		printf("Введіть кількість(номер) структур(и):  %i  ", index);
	}
	return index;
}

void ifsMenu(int indext) {
	if (indext == 0) {
		conMove(indext+4,5);
		conSetAttr(FG_RED);
		printf("StructToStr(pointer)");
		conReset();
	}
	if (indext == 1) {
		conMove(indext+4,5);
		conSetAttr(FG_RED);
		printf("StructsToTextfile (quantity)");
		conReset();
	}
	if (indext == 2) {
		conMove(indext+4,5);
		conSetAttr(FG_RED);
		printf("TextfileToStructs (quantity)");
		conReset();
	}
	if (indext == 3) {
		conMove(indext+4,5);
		conSetAttr(FG_RED);
		printf("DeleteStruct");
		conReset();
	}
	if (indext == 4) {
		conMove(indext+4,5);
		conSetAttr(FG_RED);
		printf("CopyStruct");
		conReset();
	}
	if (indext == 5) {
		conMove(indext+4,5);
		conSetAttr(FG_RED);
		printf("RewriteStructField(pointer)");
		conReset();
	}
	if (indext == 6) {
		conMove(indext+4,5);
		conSetAttr(FG_RED);
		printf("Знайти К провайдерів із найдорожчим тарифом на інтернет");
		conReset();
	}
	if (indext == 7) {
		conMove(indext+4,5);
		conSetAttr(FG_RED);
		printf("Existed structs to textfile");
		conReset();
	}
	if (indext == 8) {
		conMove(indext+4,5);
		conSetAttr(FG_RED);
		printf("Existed structs on screen");
		conReset();
	}
	if (indext == 9) {
		conMove(indext+4,5);
		conSetAttr(FG_RED);
		printf("RewriteOneStructByIndex");
		conReset();
	}
}

void backToMenu(void) {
	exitlab();
	conClear();
	conMove(4,5);
	conSetAttr(FG_RED);
	printf("StructToStr(pointer)");
	conReset();
	printTaskMenu();
}
