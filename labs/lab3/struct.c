#include <stdio.h>
#include <progbase.h>
#include <pbconsole.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "struct.h"
#include "prints.h"
enum { BUFFER_SIZE = 100 };

void createNew(char input[][30]) {
	printf("1) Назва: ");
	fgets(input[0],BUFFER_SIZE,stdin);
	input[0][strlen(input[0]) - 1] = '\0';

	printf("2) Швидкість: ");
	fgets(input[1],BUFFER_SIZE,stdin);
	input[1][strlen(input[1]) - 1] = '\0';
	
	printf("3) Тариф: ");
	fgets(input[2],BUFFER_SIZE,stdin);
	input[2][strlen(input[2]) - 1] = '\0';
	
	printf("4.1) Ім'я директора: ");
	fgets(input[3],BUFFER_SIZE,stdin);
	input[3][strlen(input[3]) - 1] = '\0';

	printf("4.2) Вік: ");
	fgets(input[4],BUFFER_SIZE,stdin);
	input[4][strlen(input[4]) - 1] = '\0';

	printf("4.3) Стать: ");
	fgets(input[5],BUFFER_SIZE,stdin);
	input[5][strlen(input[5]) - 1] = '\0';
}

void StrToStruct(struct internet_prov provider[], int j, char str[][30]) {
	strcpy(provider[j].name,str[0]);
	provider[j].speed = atoi(str[1]);
	provider[j].tariff = atof(str[2]);
	strcpy(provider[j].boss.fullname,str[3]);
	provider[j].boss.age = atoi(str[4]);
	strcpy(provider[j].boss.sex,str[5]);
}

char * StructToStr(struct internet_prov *provider, char * str) {
	sprintf(str,"%s %u %.3f %s %u %s ",provider->name,provider->speed, provider->tariff, provider->boss.fullname, provider->boss.age, provider->boss.sex);
	return str;
}

int TextToStructs(struct internet_prov providers[], int i, const char * readFileName) {
	FILE * fin = fopen(readFileName, "r+");
	if ( fin == NULL) {
		return EXIT_FAILURE;
	}
	for (int j = 0; j < i; j++) {
		fscanf(fin,"%s\n%u\n%f\n%s\n%u\n%s\n\n",providers[j].name, &providers[j].speed, &providers[j].tariff, providers[j].boss.fullname, &providers[j].boss.age, providers[j].boss.sex);
	}
	fclose(fin);
	return 0;
}

int StructsToText(struct internet_prov providers[], int i, const char * writeFileName) {
	FILE * fout = fopen(writeFileName, "w+");
	if (fout == NULL) {
		return EXIT_FAILURE;
	}
	for ( int j = 0; j < i; j++) {
		fprintf(fout,"%s\n%u біт/сек\n%.3f грн/міс\n%s\n%u\n%s\n\n", providers[j].name, providers[j].speed, providers[j].tariff, providers[j].boss.fullname, providers[j].boss.age, providers[j].boss.sex);
	}
	fclose(fout);
	return 0;
}

void DeleteStruct(struct internet_prov providers[], int j) {
	strcpy(providers[j].name," ");
	providers[j].speed = 0;
	providers[j].tariff = 0.0;
	strcpy(providers[j].boss.fullname," ");
	providers[j].boss.age = 0;
	strcpy(providers[j].boss.sex," ");
}

void RewriteStruct(struct internet_prov providers[], int i, int j) {
	providers[i] = providers[j];
}

void RewriteStructField(struct internet_prov * provider, int field, char * str) {
	switch (field) {
		case 0:
			strcpy(provider->name,str);
		break;
		case 1:
			provider->speed = atoi(str);
		break;
		case 2:
			provider->tariff = atof(str);
		break;
		case 3:
			strcpy(provider->boss.fullname,str);
		break;
		case 4:
			provider->boss.age = atoi(str);
		break;
		case 5:
			strcpy(provider->boss.sex,str);
		break;
	}
}

int cmp(const void *a, const void *b) {
	const struct internet_prov left = *(const struct internet_prov *)a; 
	const struct internet_prov right = *(const struct internet_prov *)b; 

	return (left.tariff < right.tariff) - (right.tariff < left.tariff);
}

void Top_K_Structs(struct internet_prov providers[], int k, int lim) {
	qsort(providers, lim, sizeof(struct internet_prov), cmp);
}
