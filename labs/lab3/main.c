#include <stdio.h>
#include <progbase.h>
#include <pbconsole.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "menu.h"
#include "struct.h"
#include "prints.h"
#include "test.h"

int main(int argc, char* argv[]) {
	if (argc > 1 && !strcmp(argv[1],"t")) {
		tests();
	}
	int ch = 0;
	int index = 0;
	struct internet_prov providers[10];
	menuPrint();
	while (ch != 127) {
		ch = conGetChar();
		conReset();
		conMove(3,30);
		printf("Створити новий масив даних");
		conMove(3,93);
		printf("Зчитати масив даних із файлу");
		
		index = menu(ch,1,index,68,65);
		
		if (index == 0) {
			conMove(3,30);
			conSetAttr(FG_RED);
			printf("Створити новий масив даних");
			conReset();
		}
		if (index == 1) {
			conMove(3,93);
			conSetAttr(FG_RED);
			printf("Зчитати масив даних із файлу");
			conReset();
		}
		if (ch == 10) {
			conClear();
			switch (index) {
				case 0: {
					int i = 1;
					char str[6][30];
					i = countStruct(i,10,1);
					conClear();
					conShowCursor();
					for (int j = 0; j < i;j++) {
						conMove(1,1);
						printf("Введіть структури, заповняючи поля в такому порядку: \n1) Назва:\n2) Швидкість:\n3) Тариф:\n4.1) Ім'я директора:\n4.2) Вік:\n4.3) Стать:");
						conMove(2,1);
						createNew(str);
						StrToStruct(providers, j, str);
						conClear();
					}
					funcsPrint(providers, i);
					exitlab();
					taskMenu(providers,i);
				} break;
				case 1:{
					int i = 10;
					int f = 0;
					char filename[20];
					conShowCursor();
					printf("Введіть назву файлу з якого будуть зчитані дані (structs.txt): ");
					fgets(filename,20,stdin);
					filename[strlen(filename) - 1] = '\0';
					conHideCursor();
					conClear();
					f = TextToStructs(providers,i,filename);
					if (f == 1) {
						printf("Файл пустий або не існує");
						i = 1;
						f = TextToStructs(providers,i,"empty.txt");
						providers[0].boss.age = 0;
					} else {
						funcsPrint(providers, i);
					}
					exitlab();
					taskMenu(providers,i);
				} break;
			}
		}
	}
	conClear();
	conShowCursor();
	puts("Best wishes!");
	return 0;
}
