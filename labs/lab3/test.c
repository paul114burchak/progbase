#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"
#include <stdio.h>
#include "test.h"

void tests(void) {
	testStructToStr();
	testRewriteStruct();
	testRewriteStructField();
	testStrToStruct();
}

void testStructToStr(void) {
	struct internet_prov test = {"C", 1000, 300.0, {"Ritchie",70, "male"} };
	char str1 [200];
	assert(strcmp(StructToStr(&test, str1), "C 1000 300.000 Ritchie 70 male ") == 0);
	
	struct internet_prov test2 = {"", 2016, 300.0, {"Ritchie",70, "male"} };
	char str2 [200];
	assert(strcmp(StructToStr(&test2, str2), " 2016 300.000 Ritchie 70 male ") == 0);
	
	struct internet_prov test3 = {"", 1, 301.0, {"",3, ""} };
	char str3 [200];
	assert(strcmp(StructToStr(&test3, str3), " 1 301.000  3  ") == 0);
}

void testRewriteStruct(void) {
	struct internet_prov test[2] = {
	{ "KOTIK",554, 32.7,{ "",55, "creature"}},
	{"0",0,0.0,{"0",2,"male"}}
	};
	RewriteStruct(test,1,0);
	assert(strcmp(test[0].name, test[0].name ) == 0);
	
	assert(test[1].speed == 554);
	
	assert(test[1].boss.age == 55);
	
	assert(strcmp(test[0].boss.fullname, test[1].boss.fullname ) == 0);
	
	assert(strlen(test[1].boss.fullname) == 0);
}

void testRewriteStructField(void) {
	struct internet_prov test;
	char str1[30] = "kpipzks";
	RewriteStructField(&test, 0, str1);
	assert(strcmp(test.name, str1) == 0);

	char str2[30] = "2357";
	RewriteStructField(&test, 1, str2);
	assert(test.speed == 2357);

	char str3[30] = "";
	RewriteStructField(&test, 1, str3);
	assert(test.speed == 0);

	char str4[30] = "ewfbwkm";
	RewriteStructField(&test, 2, str4);
	assert(test.tariff == 0);

	char str5[30] = "4.3";
	RewriteStructField(&test, 5, str5);
	assert(strcmp(test.boss.sex, str5) == 0);
}

void testStrToStruct(void) {
	struct internet_prov test[1];
	char str[6][30] = { {"name\0"},{"99\0"},{""},{"boss\0"},{"19\0"},{"female\0"}};
	StrToStruct(test,0,str);

	assert(strcmp(str[0],test[0].name) == 0);
	assert(strcmp(str[5],test[0].boss.sex) == 0);
	assert(test[0].speed == 99);
	assert(test[0].tariff == 0);
	assert(test[0].boss.age == 19);
}
