#include <math.h>
#include <stdio.h>
#include <progbase.h>

int main(void) 
{
	double a = 0;
	double a0 = 0;
	double a1 = 0;
	double a2 = 0;
	double x = 0;
	double y = 0;
	int yint = 0;
	double yfrac = 0;
	double z = 0;
	int zint = 0;
	double zfrac = 0;
	int id = 0;
	
	puts("Введіть Х:");
	x = getDouble();
	printf("Ви ввели: X = %.10f\n", x);
	
	puts("Введіть Y:");
	y = getDouble();
	printf("Ви ввели: Y = %.10f\n", y);
	
	puts("Введіть Z:");
	z = getDouble();
	printf("Ви ввели: Z = %.10f\n", z);
	
	yint = (int) y;
	yfrac = y - yint;
	
	zint = (int) 1/z;
	zfrac = 1/z - zint;
	
	if ( 0 == z || x-y == 0) {
		puts("Значення а0 не існує. Ділення на нуль.");
		id++;
	} else { if ( ((x-y) < 0 && (zfrac != 0 || abs(1/z) % 2 == 0)) || ( x < 0 && (yfrac != 0 || abs(y+1) % 2 == 0))) {
			puts("Значення а0 не існує. Від'ємне число під коренем.");
			id++;
		}
		a0 = pow(x,y+1)/pow(x-y,1/z);
	}
	
	
	if ( 0 == x) {
		puts("Значення а1 не існує. Ділення на нуль.");
		id++;
	} else {
		a1 = 4*y+z/x;
	}
	
	
	if ( 0 == sin(y)) {
		puts("Значення а2 не існує. Ділення на нуль.");
		id++;
	} else { if ( x < 0 ) {
			puts("Значення а2 не існує. Від'ємне число під коренем.");
			id++;
		}
		a2 = pow(x,1/fabs(sin(y)));
	}
	
	switch (id) {
	case 0:
		a = a0 + a1 + a2;
		printf("a = a0 + a1 + a2 = %.10f\n", a);
		break;
	case 1:
		puts("Значення а не може бути обчислене, оскільки один з доданків не існує");
		break;
	case 2:
		puts("Значення а не може бути обчислене, оскільки два з трьох доданків не існують");
		break;
	case 3:
		puts("Значення а не може бути обчислене, оскільки всі доданки не існують");
		break;
	}
	
	return 0;
}
