#include <math.h>
#include <stdio.h>
#include <progbase.h>

int main(void) 
{
	int a = 0;
	int b = 0;
	int op = 0;
	int res = 0;
	const double pi = 3.14159265359;
	double dres = 0;
	
	puts("Введіть ціле a:");
	a = getInt();
	printf("Ви ввели: a = %i\n", a);
	
	puts("Введіть ціле b:");
	b = getInt();
	printf("Ви ввели: b = %i\n", b);
	
	puts("Введіть код операції :");
	op = getInt();
	if ( op < 0 ) {
		op = abs(op);
		a = a + b;
		b = a - b;
		a = a - b;
		printf("Код операції: %i\n", op);
	puts("Значення а та b помінялися місцями.");
	printf("a = %i\n", a);
	printf("b = %i\n", b);
	}
	
	puts("");
	
	switch (op) {
		case 0:
			res = a + b;
			printf("Результат операції: a + b = %i\n",res);
			break;
		case 1:
			res = a - b;
			printf("Результат операції: a - b = %i\n",res);
			break;
		case 2: 
			res = a * b;
			printf("Результат операції: a * b = %i\n",res);
			break;
		case 3:
			if (b == 0) {
				puts("Результат операції: a / b неможливо обчислити. Ділення на нуль."); 
			} else { res = a / b;
				printf("Результат операції: a / b = %i\n",res);
			}
			break;
		case 4:
			res = abs(a);
			printf("Результат операції: abs(a) = %i\n",res);
			break;
		case 5:
			res = a;
			if (b < res) {
				res = b;
			}
			printf("Результат операції: min(a,b) = %i\n",res);
			break;
		case 6:
			res = a;
			if (b > res) {
				res = b;
			}
			printf("Результат операції: max(a,b) = %i\n",res);
			break;
		case 7:
		case 13:
			res = pow(a,b);
			printf("Результат операції: a^b = %i\n",res);
			break;
		case 8: 
			if (a == 0) {
				puts("Результат операції: var(a,b) неможливо обчислити. Ділення на нуль.");
			} else {
				dres = 3.0 * cos( b * pi )/ a;
				printf("Результат операції: var(a,b) = %f\n",dres);
			}
			break;
		default: 
			if (a == -1 || b == -1) {
				puts("Операцію неможливо виконати: ділення по модулю на 0");
			} else {
				res = (op % (abs(a+1))) + (op % (abs(b+1)));
				printf("Результат операції: def(a,b) = %i\n",res);
			}
			break;
	}
	return 0;
}
