#include <math.h>
#include <stdio.h>
#include <progbase.h>

int main(void) 
{
	unsigned int m = 0;
	unsigned int n = 0;
	int i = 0;
	int j = 0;
	float x = 0;
	
	puts("Введіть натуральне число n:");
	n = getInt();
	printf("Ви ввели: n = %i\n", n);
	
	puts("Введіть натуральне число m:");
	m = getInt();
	printf("Ви ввели: m = %i\n", m);
	
	
	for (i = 1; i <= n; i++) {
		for (j = 1; j <= m; j++) {
			x = x + pow(i + 2 * j, 0.5);
		}
	}
	printf("Х = %f\n", x);
	
	return 0;
}
